#!/usr/bin/env python3 

import urllib.request
import urllib.parse
import json
import argparse
import os

class EnvDefault(argparse.Action):
    def __init__(self, envvar, required=True, default=None, **kwargs):
        if not default and envvar:
            if envvar in os.environ:
                default = os.environ[envvar]
        if required and default:
            required = False
        super(EnvDefault, self).__init__(default=default, required=required, **kwargs)
    def __call__(self, parser, namespace, values, option_string=None):
        setattr(namespace, self.dest, values)

parser = argparse.ArgumentParser(description="Octopus API functions.")
parser.add_argument('-t', '--token', help='Octopus API token', envvar='OCTOPUS_TOKEN', type=str, action=EnvDefault)
args = parser.parse_args()

base_url = "https://api.octopus.energy/v1/"

password_manager = urllib.request.HTTPPasswordMgrWithDefaultRealm()

password_manager.add_password(realm=None, uri=base_url, user=args.token, passwd='')

auth_handler = urllib.request.HTTPBasicAuthHandler(password_manager)
opener = urllib.request.build_opener(auth_handler)

urllib.request.install_opener(opener)

if __name__ == "__main__":
    data = {}
    data["is_variable"] = "true"
    url_values = urllib.parse.urlencode(data)
    full_url = base_url + 'products' + '?' + url_values
    with urllib.request.urlopen(full_url) as data:
        body = data.read()

    parsed = json.loads(body)

    print(json.dumps(parsed, indent=4))